# Facebook News Feed Clone

Given as an assessment exam for MRM. Static clone of a Facebook page timeline look.

Project is built with [TailwindCSS](https://tailwindcss.com), [PostCSS](https://postcss.org/), [Rollup](https://rollupjs.org/) and [Svelte](https://svelte.dev).

## Live Demo

[CLICK HERE](https://aaronreal.gitlab.io/fb-news-feed-clone/)

## Get started

Clone the project

```bash
git clone git@gitlab.com:aaronreal25/fb-news-feed-clone.git
cd fb-news-feed-clone
```

*Note that you will need to have [Node.js](https://nodejs.org) installed.*

Install the dependencies:

```bash
cd fb-news-feed-clone
yarn # or npm i
```

Then after completion, run:

```bash
yarn dev # or npm run dev
```

Type [localhost:5000](http://localhost:5000) or open `public/index.html` on your browser to view the output

## Folder Structure

```
├── README.md
├── package.json
├── postcss.config.js       # Handles all css processing
├── public
│   ├── images
│   ├── build
│   │   ├── bundle.css      # Bundled css (minified for production)
│   │   ├── bundle.css.map
│   │   ├── bundle.js       # Bundled js (minified for production)
│   │   └── bundle.js.map
│   ├── favicon.png
│   └── index.html          # What the user sees on the browser
├── rollup.config.js        
├── components              # Source code of all the components
├── main
│   ├── Desktop.svelte
│   └── Mobile.svelte      
├── src
│   ├── App.svelte          
│   ├── main.css            
│   └── main.js             
├── tailwind.config.js      # https://tailwindcss.com/docs/configuration/
└── yarn.lock
```


## Building and running in production mode

To create an optimised version (minified assets and markup) of the app:

```bash
yarn build # or npm run build
```
