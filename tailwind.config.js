module.exports = {
  theme: {
    extend: {
    	colors: {
    		'fb-darkblue': '#3b5998',
        'fb-darkestblue': '#293e6a',
        'fb-lightblue': '#4080ff',
        'fb-blue': '#4267b2',
    		'fb-lightgray': '#9f9ba3',
        'fb-gray': '#7f8594',
        'fb-darkerblue': '#17233d',
        'fb-background': '#E9EBEE',
        'fb-tabs-hover': '#dee0e3',
        'fb-green': '#42b72a',
        'fb-faded-orange': '#f7b47a',
        gray: {
          '100': '#F5F6F7',
          '200': '#ebedf0',
          '300': '#ccd0d5',
          '400': '#afb4bd',
          '500': '#656c7a',
          '700': '#4e5665',
        }
    	}
    },
    fontFamily: {
      'serif': ['Roboto', '"Droid Sans"', 'Helvetica', 'sans-serif'],
    }
  },
  variants: {},
  plugins: [
    function ({ addComponents }) {
      addComponents({
        '.container': {
          width: '100%',
          // marginLeft: 'auto',
          // marginRight: 'auto',
          // paddingLeft: '2rem',
          // paddingRight: '2rem',
          '@screen sm': {
            maxWidth: '640px',
          },
          '@screen md': {
            maxWidth: '768px',
          },
          '@screen lg': {
            maxWidth: '1024px',
          },
          '@screen xl': {
            maxWidth: '1024px',
          },
        }
      })
    }
  ]
}
